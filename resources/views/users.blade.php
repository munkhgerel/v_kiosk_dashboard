@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-11">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Хэрэглэгчийн нэр</th>
                    <th scope="col">Овог</th>
                    <th scope="col">Нэр</th>
                    <th scope="col">И-мэйл хаяг</th>
                    <th scope="col">Утасны дугаар</th>
                    <th scope="col">Эрх</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>@if($user->role == 1) Админ @elseif($user->role == 2) Цэнэглэгч @endif</td>
                        <td>
                            <button class="btn-outline-danger btn-sm"><i class="fa fa-trash"></i></button>
                            <button class="btn-outline-info btn-sm"><i class="fa fa-edit"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-1">
            <button class="btn-outline-success btn-sm float-right"><i class="fa fa-plus"></i></button>
        </div>
    </div>
    <div class="row"></div>
@endsection
